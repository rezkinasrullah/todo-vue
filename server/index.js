var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var dotenv = require('dotenv').config();
const bodyParser = require('body-parser')
const cors = require('cors')
var fs = require('fs')
//const twitterApp = require('./models/v1/Twitter')

// var todosRouter = require('./routes/todos');
// var categoriesRouter = require('./routes/categories')

var app = express();
const apiPort = process.env.PORT || 3000

// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(bodyParser.json())
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

// Routing group
var apiRouter = require('./routes')

app.get('/', function(req, res, next){ res.send("Welcome to Express API") })
app.use('/api', apiRouter)


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

//twitterApp();

module.exports = app;

app.listen(apiPort, () => console.log(`Server running on port ${apiPort}`))