var express = require('express')

var router = express.Router()
var todosRouter = require('./todos')
router.use('/todos', todosRouter)

module.exports = router;