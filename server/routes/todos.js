var express = require('express');
var router = express.Router();
var todoController = require("../controller/todoController")
router.get('/', todoController.index);
router.post('/', todoController.store);
router.get('/:id', todoController.show);
router.put('/:id', todoController.update);
router.delete('/:id', todoController.destroy);
module.exports = router;
