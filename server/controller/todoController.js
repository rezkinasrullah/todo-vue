var db = require('../config/database')
exports.index = async (req, res, next) => {
   try {
      let todos = await db('todos')
                        .where((builder) => {
                           if(req.query.q)
                           {
                              builder.where('title', 'LIKE', '%'+req.query.q+'%')
                           }
                        })
                        // .paginate(10, req.query.page || 1, true)
      res.status(200).json({"error": false, "message": "Success get data", "data": todos})
   } catch (e) {
      console.log(e);
      res.status(500).json({"error": true, "message": e.message, "data": null})
   }
}

exports.show = async (req, res, next) => {
   try {
      let todo = await db('todos')
                        .where('id', req.params.id)
                        .first()
      res.status(200).json({"error": false, "message": "Success get data", "data": todo})
   } catch (e) {
      console.log(e);
      res.status(500).json({"error": true, "message": e.message, "data": null})
   }
}

exports.store = async (req, res, next) => {
    var data = req.body
    try{
        var todo = await db('todos')
                .insert(req.body);
        res.status(200).json({"error": false, "message": "Sukses insert data", "data": todo[0]})
    }catch(e){
        console.error(e)
        res.status(500).json({"error": true, "message": e.message, "data": null})
    }
}


exports.update = async (req, res, next) => {
   try {
      var id = req.params.id
      let todo = await db('todos')
                        .where('id', id)
                        .update(req.body)

      todo = await db('todos')
                        .where('id', req.params.id)
                        .first()  
                                     
      res.status(200).json({"error": false, "message": "Success update data", "data": todo})
   } catch (e) {
      console.log(e);
      res.status(500).json({"error": true, "message": e.message, "data": null})
   }
}

exports.destroy = async (req, res, next) => {
   try {
      var id = req.params.id
      let todo = await db('todos')
                        .where('id', id)
                        .delete()
      res.status(200).json({"error": false, "message": "Success delete data", "data": todo})
   } catch (e) {
      console.log(e);
      res.status(500).json({"error": true, "message": e.message, "data": null})
   }
}