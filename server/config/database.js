var knex = require('knex')({
    client: 'mysql',
    version: '5.7',
    connection: {
      host : process.env.DB_HOST,
      user : process.env.DB_USER,
      password : process.env.DB_PASS,
      database : process.env.DB_NAME
    }
});

// const setupPaginator = require('knex-paginator')
// setupPaginator(knex)

module.exports = knex