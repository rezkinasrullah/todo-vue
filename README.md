# vuextodo

## Project setup
1. Run this command to install the depedencies
```
npm install
```
2. Change API URL on .env on root dir

### Compiles and hot-reloads for development
1. For running in development mode run
```
npm run serve
```
2. Then open localhost:8080 for development mode

### Compiles and minifies for production
1. Run command to build
```
npm run build
```
2. Upload folder dist into your server

### API Server 

1. Deploy .sql into created database
2. Change .env on server folder
3. Open folder server then run 
```
node index.js
```
4. Server is running if there is no error 

