import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
Vue.use(Vuex)
export default new Vuex.Store({
 state: {
   todos: [],
   newTodo: '',
   todoId: 0,
 },
 mutations: {
    SET_TODOS(state, todos){
      state.todos = todos
    },
    ADD_TODO(state, todo){
      state.newTodo = ''
      state.todoId = 0
      state.todos.push({
          title: todo,
          completed: 0
      })
    },
    UPDATE_TODO(state, todo){
      var indexTodo = state.todos.findIndex((n) => n.id == todo.id)
      var todos = state.todos
      todos.splice(indexTodo, 1, todo)
      state.todos = todos
      state.newTodo = ''
      state.todoId = 0
    },
    REMOVE_TODO(state, todo){
      var todos = state.todos
      todos.splice(todos.indexOf(todo), 1)
      state.todos = todos
    },
    SET_TODO_TITLE(state, title){
      state.newTodo = title
    },
    SET_TODO_ID(state, id){
      state.todoId = id
    },
  },
 actions: {
    updateTodoTitle({commit}, title){
        commit('SET_TODO_TITLE', title);
    },
    updateTodoId({commit}, id){
        commit('SET_TODO_ID', id);
    },
    getTodo({commit}){
      console.log(process.env.VUE_APP_ROOT_API);
      axios.get(process.env.VUE_APP_ROOT_API + '/api/todos')
          .then(response => {
              commit('SET_TODOS', response.data.data)
      })
    },

    saveTodo({commit, dispatch}, title){
      axios.post(process.env.VUE_APP_ROOT_API + '/api/todos', {title: title})
          .then(response => {
            if(!response.data.error){
              commit('ADD_TODO', title)
              dispatch('getTodo')
            }
      })
    },
    updateTodo({commit}, todo){
      axios.put(process.env.VUE_APP_ROOT_API + '/api/todos/' + todo.id, {title: todo.title})
          .then(response => {
            if(!response.data.error){
              commit('UPDATE_TODO', response.data.data)
            }
      })
    },

    deleteTodo({commit}, todo){
      axios.delete(process.env.VUE_APP_ROOT_API + '/api/todos/' + todo.id)
          .then(response => {
            if(!response.data.error){
              commit('REMOVE_TODO', todo)
            }
      })
    },
    completeTodo({commit}, todo){
      axios.put(process.env.VUE_APP_ROOT_API + '/api/todos/' + todo.id, {completed: 1})
          .then(response => {
            if(!response.data.error){
              commit('UPDATE_TODO', response.data.data)
            }
      })
    },
    uncompleteTodo({commit}, todo){
      axios.put(process.env.VUE_APP_ROOT_API + '/api/todos/' + todo.id, {completed: 0})
          .then(response => {
            if(!response.data.error){
              commit('UPDATE_TODO', response.data.data)
            }
      })
    },
  },
  getters: {
    newTodo: state => state.newTodo,
    todos: state => state.todos.filter((todo) => {return todo.completed !== 1}),
    completedTodos: state => state.todos.filter((todo) => {return todo.completed === 1})
  }
})